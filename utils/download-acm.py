import requests
import xml.etree.ElementTree as ET
import urllib.parse
import re
import csv
from bs4 import BeautifulSoup

def download_and_extract_titles(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        tree = ET.fromstring(response.content)
        titles = [title.text for title in tree.findall('.//title')]
        return titles
    except requests.RequestException as e:
        print(f"Error fetching the XML: {e}")
        return []
    except ET.ParseError as e:
        print(f"Error parsing the XML: {e}")
        return []

def generate_search_url(title):
    encoded_title = urllib.parse.quote(title)
    search_url = "https://www.google.com/scholar?q={title}".format(title=encoded_title)
    return  search_url

# def get_paper_link(search_url):
#     try:
#         response = requests.get(search_url, headers = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'})
#         response.raise_for_status()

#         soup = BeautifulSoup(response.text, 'html.parser')
#         pdf_link = soup.find_all('a', href=True, string=re.compile('dl.acm.org'))

#         if pdf_link:
#             href = pdf_link['href']
#             return href
#         else:
#             print("[-] No PDF link found.")
#             return ''
#     except requests.RequestException as e:
#         print(f"Error fetching the PDF link: {e}")
#         return []
#     except ET.ParseError as e:
#         print(f"Error parsing the PDF link: {e}")
#         return []

# URL of the XML file
xml_url = "https://www.sigsac.org/ccs/CCS2023/assets/data/accepted_papers.xml?ver=2024-05-19"
titles = download_and_extract_titles(xml_url)

f = open('acm.csv', 'w')
writer = csv.writer(f, quoting=csv.QUOTE_ALL)
header = ['Num','Paper Title','Author','Affiliation','Conference','Year','Link to Paper','Link for Code','Available','ReadMe','RepoGoal','TrainedModel','Out-of-box','Runs','Output','Data_Available','Train/test','Reason','Model Used','Hyperparameters','Training Described']
writer.writerow(header)

# Write out base conference informations with the google scholar titel
i = 0;
for title in titles:
    search_url = generate_search_url(title)
    data = [i, title, '','','CCS', '2023', search_url, '', '', '', '', '', '', '', '', '', '', '', '', '', '']
    writer.writerow(data)
    i += 1
