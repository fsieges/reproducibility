import requests
import xml.etree.ElementTree as ET
import urllib.parse
import re
import csv
from bs4 import BeautifulSoup


def get_paper_title(search_url):
    papers = {}
    try:
        response = requests.get(search_url, headers = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'})
        response.raise_for_status()

        soup = BeautifulSoup(response.text, 'html.parser')
        items = soup.find_all('div', {"class" :"field-item"})

        if items:
            for item in items:
                #pt = item.find('div', {"class":"field-item"})
                paper = item.find('article')
                paper_title_link = ''
                paper_author = ''
                paper_attr = []
                if paper:
                    paper_title_heading = paper.find('h2',  {"class" :"node-title"})
                    paper_title_link = paper_title_heading.find('a')
                    paper_author = paper.find('p')
                if paper_title_link and paper_author:
                    # print(paper_title_link.text)
                    # print(paper_title_link.get('href'))
                    # print(paper_author.text)

                    # Get authors
                    paper_attr.append(paper_author.text.strip())

                    # Get paper link from details
                    final_paper_link = get_paper_link('https://www.usenix.org' + paper_title_link.get('href'))
                    paper_attr.append(final_paper_link)

                    # add paper and it's attributes to dict
                    papers[paper_title_link.text.strip()] = paper_attr
            return papers
        else:
            print("[-] No item found.")
            return ''
    except requests.RequestException as e:
        print(f"Error fetching the item: {e}")
        return ''

def get_paper_link(usenix_url):
    try:
        response = requests.get(usenix_url, headers = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'})
        response.raise_for_status()

        soup = BeautifulSoup(response.text, 'html.parser')
        final_paper = soup.find('div', {"class" : "field-name-field-final-paper-pdf"})

        if final_paper:
            print(final_paper.find('a').get('href'))
            return final_paper.find('a').get('href')
        else:
            print("[-] No item found.")
            return ''
    except requests.RequestException as e:
        print(f"Error fetching the item: {e}")
        return ''


# Conference URL
data_urls = ["https://www.usenix.org/conference/usenixsecurity23/summer-accepted-papers", "https://www.usenix.org/conference/usenixsecurity23/fall-accepted-papers"]


f = open('usenix.csv', 'w')
writer = csv.writer(f, quoting=csv.QUOTE_ALL)
header = ['Num','Paper Title','Author','Affiliation','Conference','Year','Link to Paper','Link for Code','Available','ReadMe','RepoGoal','TrainedModel','Out-of-box','Runs','Output','Data_Available','Train/test','Reason','Model Used','Hyperparameters','Training Described']
writer.writerow(header)
for url in data_urls:
    print("[+] Get papers for URL:")
    papers = get_paper_title(url)
    i = 0;
    print("[+] Write papers to csv")
    for paper in papers:
        print(i, paper, papers[paper][0],'','Usenix', '2023', papers[paper][1])
        data = [i, paper, papers[paper][0],'','Usenix', '2023', papers[paper][1], '', '', '', '', '', '', '', '', '', '', '', '', '', '']
        writer.writerow(data)
        i += 1




