import requests
import xml.etree.ElementTree as ET
import urllib.parse
import re
import csv
from bs4 import BeautifulSoup


def generate_search_url(title):
    encoded_title = urllib.parse.quote(title)
    search_url = "https://www.google.com/scholar?q={title}".format(title=encoded_title)
    return  search_url

def get_paper_link(search_url):
    papers = {}
    try:
        response = requests.get(search_url, headers = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'})
        response.raise_for_status()

        soup = BeautifulSoup(response.text, 'html.parser')
        titles = soup.find_all('div', {"class" :"list-group-item"})

        if titles:
            for title in titles:
                pt = title.find('b').text.strip()
                at = title.find('br').next_sibling
                if at:
                    at = at.strip()
                    cleaned_text = re.sub(r'\s*\([^)]*\)', '', at)
                    papers[pt] = cleaned_text;
            return papers
        else:
            print("[-] No title found.")
            return ''
    except requests.RequestException as e:
        print(f"Error fetching the title: {e}")
        return ''

# Conference URL
data_url = "https://www.ieee-security.org/TC/SP2023/program-papers.html"
papers = get_paper_link(data_url)

f = open('ieee-sp.csv', 'w')
writer = csv.writer(f, quoting=csv.QUOTE_ALL)
header = ['Num','Paper Title','Author','Affiliation','Conference','Year','Link to Paper','Link for Code','Available','ReadMe','RepoGoal','TrainedModel','Out-of-box','Runs','Output','Data_Available','Train/test','Reason','Model Used','Hyperparameters','Training Described']
writer.writerow(header)

# Write out base conference informations with the google scholar titel
i = 0;
for paper in papers:
    search_url = generate_search_url(paper)
    data = [i, paper, papers[paper],'','S&P', '2023', search_url, '', '', '', '', '', '', '', '', '', '', '', '', '', '']
    writer.writerow(data)
    i += 1
