#!/usr/bin/env python3
import pandas as pd
import argparse

"""
Small helper script to normalize the collected data ({conference}-sorted.csv) before it is merged with the list of results.
This script needs to be placed in the same directory as your data.
"""

def normalize(conference, line_number):
  df = pd.read_csv("{}-sorted.csv".format(conference))

  print("[!] Brief overview of the original data.")
  print(df)

  print("\n[/] Normalize data.\n")
  row = 0
  num = line_number
  for colrow in df['Num']:
    num += 1
    df.loc[row, 'Num'] = num
    # Those values are not evaluated hence -1
    df.loc[row, 'Affiliation'] = -1
    df.loc[row, 'ReadMe'] = -1
    df.loc[row, 'RepoGoal'] = -1
    df.loc[row, 'TrainedModel'] = -1
    df.loc[row, 'Out-of-box'] = -1
    df.loc[row, 'Runs'] = -1
    df.loc[row, 'Output'] = -1
    row += 1

  # Set columns as int
  df['Num'] = df['Num'].fillna(-1)
  df['Num'] = df['Num'].astype('int64')

  df['Year'] = df['Year'].fillna(-1)
  df['Year'] = df['Year'].astype('int64')

  df['Available'] = df['Available'].fillna(-1)
  df['Available'] = df['Available'].astype('int64')

  df['ReadMe'] = df['ReadMe'].fillna(-1)
  df['ReadMe'] = df['ReadMe'].astype('int64')

  df['RepoGoal'] = df['RepoGoal'].fillna(-1)
  df['RepoGoal'] = df['RepoGoal'].astype('int64')

  df['TrainedModel'] = df['TrainedModel'].fillna(-1)
  df['TrainedModel'] = df['TrainedModel'].astype('int64')

  df['Out-of-box'] = df['Out-of-box'].fillna(-1)
  df['Out-of-box'] = df['Out-of-box'].astype('int64')

  df['Runs'] = df['Runs'].fillna(-1)
  df['Runs'] = df['Runs'].astype('int64')

  df['Output'] = df['Output'].fillna(-1)
  df['Output'] = df['Output'].astype('int64')


  # Those colums are float
  df['Affiliation'] = df['Affiliation'].astype('float')
  df['Train/test'] = df['Train/test'].astype('float')
  df['Reason'] = df['Reason'].astype('float')
  df['Hyperparameters'] = df['Hyperparameters'].astype('float')
  df['Training Described'] = df['Training Described'].astype('float')

  print("[!] Brief overview of the normalized data.")
  print(df)

  df.to_csv("{}-normalized.csv".format(conference), index=False)
  print("[+] Wrote normalized data to: {}-normalized.csv".format(conference))


def get_arguments():
  parser = argparse.ArgumentParser(description='Normalize the csv for merging.')
  parser.add_argument('conference', type=str, help='conference name to normalize')
  parser.add_argument('line_number', type=int, help='line number to start counting rows')
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()

  if args.conference and args.line_number:
    normalize(args.conference, args.line_number)
  else:
    print("[-] Please provide conference name to normalize the collected data and the line number to start counting.")

if __name__ == "__main__":
  main()
